import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import ElementPlus from "element-plus";
import "element-plus/lib/theme-chalk/index.css";
// import axios from "axios";
// 对axios 在进行一层封装，所以导入axios改用自己封装的http
import axios from "@/http";

const app = createApp(App);
app.use(store);
app.config.globalProperties.$axios = axios; // 全局挂在axios 在原型上 就可以直接在ctx中使用
app.use(ElementPlus);
app.use(router);
app.mount("#app");
