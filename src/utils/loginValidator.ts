import { ref, getCurrentInstance } from "vue";

interface User {
  email: string;
  password: string;
}
interface Rules {
    email: {
        type: string;
        message: string;
        required: boolean;
        trigger: string;
    }[];
    password: (
        | {
              required: boolean;
              message: string;
              trigger: string;
              min?: undefined;
              max?: undefined;
          }
        | {
              min: number;
              max: number;
              message: string;
              trigger: string;
              required?: undefined;
          }
    )[];
}

// 类型匹配<User>
export const loginUser = ref<User>({
  email: "",
  password: "",
});
// 表单校验规则
export const rules = ref<Rules>({
    email: [
        {
            type: "email",
            message: "邮箱格式不正确！",
            required: true,
            trigger: "blur",
        },
    ],
    password: [
        { required: true, message: "密码不正确！", trigger: "blur" },
        { min: 6, max: 20, message: "密码长度为6到20位...", trigger: "blur" },
    ],
});
